﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Feed
    {
        public Uri Id { get; private set; }

        public IEnumerable<IFeedItem> Items { get; set; }

        public Feed(Uri id)
        {
            Id = id;
        }
    }
}
