﻿using System;

namespace Domain.Models
{
    public class FeedItem : IFeedItem
    {
        public Uri Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public DateTime PublishDate { get; set; }
    }

    public interface IFeedItem
    {
        Uri Id { get; }
        string Title { get;  }
        string Summary { get;  }
        DateTime PublishDate { get; }
    }
}