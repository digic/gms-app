﻿using Domain.Models;
using System;
using System.ServiceModel.Syndication;

namespace Domain.Application
{
    public interface IFeedService
    {
        Feed GetFeed(Uri id);
    }
}