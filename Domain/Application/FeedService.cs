﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Domain.Application
{
    public class FeedService : IFeedService
    {
        public Feed GetFeed(Uri id)
        {
            var feed = new Feed(id);

            try
            {
                using (XmlReader reader = XmlReader.Create(id.AbsoluteUri))
                {
                    SyndicationFeed sfeed = SyndicationFeed.Load(reader);

                    feed.Items = sfeed.Items.Select(i => new FeedItem()
                    {
                        Id = new Uri(i.Id),
                        Title = i.Title.Text,
                        Summary = i.Summary.Text,
                        PublishDate = i.PublishDate.UtcDateTime
                    });
                }
            }
            catch (Exception ex)
            {
                //log exception 
            }

            return feed;
        }
    }
}
