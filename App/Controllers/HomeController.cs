﻿using App.Models;
using App.Services;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FeedItemVM = App.Models.FeedItem; // should break contexts better

namespace App.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFeedReaderService _service;
        const string FEED_URL = "http://www.givemesport.com/rss.ashx";
        const int NUMBER_OF_POSTS = 10;

        public HomeController(IFeedReaderService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = new FeedReaderViewModel();

            var feedItems = _service.GetFeedItems(FEED_URL, NUMBER_OF_POSTS);

            model.LatestNews = MapFeedItems(feedItems.Take(5));
            model.FeedItems = MapFeedItems(feedItems.Skip(5));
           
            model.LastUpdated = DateTime.UtcNow;

            return View(model);
        }

        [HttpPost]
        public ActionResult RefreshFeed()
        {
            var model = new FeedReaderViewModel();

            var feedItems = _service.GetFeedItems(FEED_URL, NUMBER_OF_POSTS);

            model.LatestNews = MapFeedItems(feedItems.Take(5));
            model.FeedItems = MapFeedItems(feedItems.Skip(5));

            model.LastUpdated = DateTime.UtcNow;

            return Json(model);
        }


        private List<FeedItemVM> MapFeedItems(IEnumerable<IFeedItem> feedItems)
        {
            return feedItems.Select(i => new FeedItemVM()
            {
                Id = i.Id.AbsoluteUri,
                Title = i.Title,
                Summary = i.Summary,
                PublishDate = i.PublishDate

            }).ToList();
        }
    }
}