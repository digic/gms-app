﻿using Domain.Application;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Services
{
    public class FeedReaderService : IFeedReaderService
    {
        private readonly IFeedService _service;

        public FeedReaderService(IFeedService service)
        {
            _service = service;
        }

        public IEnumerable<IFeedItem> GetFeedItems(string url, int numberOfPosts)
        {
            return GetFeedItems(url).OrderByDescending( i => i. PublishDate).Take(numberOfPosts);
        }

        public IEnumerable<IFeedItem> GetFeedItems(string url)
        {
            var items = Enumerable.Empty<IFeedItem>();
            try
            {
                var feed = _service.GetFeed(new Uri(url));
                if (feed.Items != null)
                {
                    items = feed.Items;
                }

            }
            catch (Exception ex)
            {
                //log
            }

            return items;
        }
    }

    public interface IFeedReaderService
    {
        IEnumerable<IFeedItem> GetFeedItems(string url, int numberOfPosts);
        IEnumerable<IFeedItem> GetFeedItems(string url);
    }
}