﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public class FeedReaderViewModel
    {
        public DateTime LastUpdated { get; set; }

        public List<FeedItem> FeedItems { get; set; }

        public List<FeedItem> LatestNews { get; set; }

    }

    public class FeedItem 
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public DateTime PublishDate { get; set; }
    }
}