﻿
(function (win, $, undefined) {

    function pad(n) { return n < 10 ? '0' + n : n; }

    function ToJavaScriptDate(value) {
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(value);
        var dt = new Date(parseFloat(results[1]));
        return pad(dt.getDate()) + "/" + pad(dt.getMonth()+ 1)+ "/"  + dt.getFullYear() + " " + pad(dt.getHours()) + ":" + pad(dt.getMinutes()) + ":" + pad(dt.getSeconds());
    }

    function reloadList(items, listId)
    {
        var list = $("#" + listId);
        list.empty();
        $.each(items, function (i, item) {
            list.append('<li><a href="' + item.Id + '">' + item.Title + '</a><span style= "display:block;"> ' + ToJavaScriptDate(item.PublishDate) + '</span></li>');
        });

    }

    function updateFeedLastUpdated(dt)
    {
        $(".updated").empty().append("Last Updated: " + ToJavaScriptDate(dt));
    }

    var refreshFeeds = function () {

        $.ajax({
            type: "POST",

            url: '/Home/RefreshFeed',
            success: function (result) {
                reloadList(result.LatestNews, "latest-news");
                reloadList(result.FeedItems, "newsfeed");

                updateFeedLastUpdated(result.LastUpdated);
            },
            error: function (xhr) {
                //debugger;  
                console.log(xhr.responseText);
                //alert("Error has occurred..");
            }
        });
    };

    win.setInterval(refreshFeeds, 5000);

})(window, jQuery);
