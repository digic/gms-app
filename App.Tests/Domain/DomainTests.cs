﻿using System;
using System.ServiceModel.Syndication;
using Domain.Application;
using Domain.Models;
using NUnit.Framework;
using System.Linq;

namespace App.Tests.Domain
{
    [TestFixture]
    public class DomainTests
    {
        //[TestCase("http://www.givemesport.com/rss.ashx")]
        //public void Should_Retrieve_SyndicationFeed(string url)
        //{
        //    var result = new FeedService().GetFeed(new Uri(url));
        //    Assert.IsInstanceOf(typeof(SyndicationFeed), result);
        //}


        [TestCase("http://www.givemesport.com/rss.ashx")]
        [TestCase("http://www.google.com/")]
        public void Should_Retrieve_Feed(string url)
        {           
            var result = new FeedService().GetFeed(new Uri(url));
           
            Assert.IsNotNull(result);
        }

        [TestCase("http://www.givemesport.com/rss.ashx", true)]
        [TestCase("http://www.google.com/", false)]
        public void Should_Retrieve_Feed_With_Items_Expected(string url, bool expected)
        {
            var feed = new FeedService().GetFeed(new Uri(url));

            var result = feed.Items != null && feed.Items.Any();

            Assert.AreEqual(expected, result);
        }
    }
}
