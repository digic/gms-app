﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Services;
using Domain.Application;
using Domain.Models;
using Moq;
using NUnit.Framework;

namespace App.Tests.Services
{
    [TestFixture]
    public class FeedReaderTests
    {
        [TestCase("http://www.givemesport.com/rss.ashx")]        
        public void Feed_Reader_Should_Retreive_Feed(string url)
        {
            var feed = new Feed(new Uri(url));
            feed.Items = Enumerable.Empty<IFeedItem>();
            var service = new Mock<IFeedService>();
            service.Setup(s => s.GetFeed(new Uri(url))).Returns(feed);

            var reader = new FeedReaderService(service.Object);
            var result = reader.GetFeedItems(url);

            Assert.IsNotNull(result);
            
        }


        [TestCase("http://www.givemesport.com/rss.ashx", 10)]
        public void Feed_Reader_Should_Retreive_Feed_NumberOfPosts_Expected(string url, int numberOfPosts)
        {
            var feed = new Feed(new Uri(url));
            var list = new List<IFeedItem>();
            for (int i = 0; i < numberOfPosts; i++)
            {
                list.Add(new FeedItem()
                {
                    Id = new Uri(url),
                    Title = "Test Item",
                    Summary = "Summary",
                    PublishDate = DateTime.UtcNow
                });
            }

            feed.Items = list;
            var service = new Mock<IFeedService>();
            service.Setup(s => s.GetFeed(new Uri(url))).Returns(feed);

            var reader = new FeedReaderService(service.Object);
            var result = reader.GetFeedItems(url, numberOfPosts);

            Assert.AreEqual(numberOfPosts, result.Count());

        }
    }
}
