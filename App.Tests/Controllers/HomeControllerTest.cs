﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using App;
using App.Controllers;
using App.Models;
using App.Services;
using Domain.Models;
using Moq;
using NUnit.Framework;

namespace App.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void Index()
        {
            // Arrange
            var service = new Mock<IFeedReaderService>();
            HomeController controller = new HomeController(service.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull((FeedReaderViewModel)result.Model);
        }

        [TestCase("http://www.givemesport.com/rss.ashx")]
        public void Index_Should_Have_Feed_Items(string url)
        {
            // Arrange
            var items = new List<IFeedItem>();
            var service = new Mock<IFeedReaderService>();
            service.Setup(s => s.GetFeedItems(url)).Returns(items);
            HomeController controller = new HomeController(service.Object);

            // Act
            ViewResult view = controller.Index() as ViewResult;
            var result = ((FeedReaderViewModel)view.Model).FeedItems;

            // Assert
            Assert.IsNotNull(result);
           
        }

    }
}
